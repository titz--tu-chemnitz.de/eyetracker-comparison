# Instructions

If you just want to load the data, run: 
```R 
d <- readRDS("data.RData")
```

The normal "load" command will not work, because the data was saved with 
saveRDS.

If you want to run the scripts, you need to install some libraries first:

```R 
install.packages(c("dplyr", "nlme", "MuMIn", "stargazer", "Unicode", "ggplot2", 
                   "Cairo", "Rmisc", "data.table"))
```

Then you can source the scripts.

Depending on your system, it can take some time to create the output files.
